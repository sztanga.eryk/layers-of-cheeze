using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TmpAudio : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            AudioManager.PlayOnce("alarm");
        }
        if (Input.GetMouseButtonDown(1))
        {
            AudioManager.PlayLooped("crow");
        }
        if (Input.GetMouseButtonDown(2))
        {
            AudioManager.Stop("crow");
        }

    }
}
