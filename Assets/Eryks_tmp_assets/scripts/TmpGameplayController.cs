using System;
using Enemy;
using UnityEngine;

namespace ErykTmp
{
    public class TmpGameplayController : MonoBehaviour
    {
        public static TmpGameplayController Instance { get; private set; }
        public GameObject Actor;
        public GameObject Ball;

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this);
            }
            else
            {
                Instance = this;
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                var mousePos = Input.mousePosition;
                mousePos.z = Camera.main.nearClipPlane;
                var worldPosition = Camera.main.ScreenToWorldPoint(mousePos);
                LeanTween.move(Actor, worldPosition, (Actor.transform.position - worldPosition).magnitude / 10);
            }

            if (Input.GetMouseButtonDown(1))
            {
                // var mousePos = Input.mousePosition;
                // mousePos.z = Camera.main.nearClipPlane;
                // var worldPosition = Camera.main.ScreenToWorldPoint(mousePos);
                // Actor.transform.position = worldPosition;
                Vector2 direction = Ball.transform.position - Actor.transform.position;
                direction = direction.normalized;
                Ball.GetComponent<BallBehaviour>().BecomeChaotic(direction);
            }
        }
    }
}