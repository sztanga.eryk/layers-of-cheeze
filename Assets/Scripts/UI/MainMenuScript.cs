using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class MainMenuScript : MonoBehaviour
{
    public static MainMenuScript Instance { get; private set; }
    [SerializeField]
    private string _levelToPlay;

    [SerializeField]
    private GameObject _mainScreen;
    [SerializeField]
    private GameObject _instructions;
    [SerializeField]
    private GameObject _credits;


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }


    private void Start()
    {
        SeeMainScreen();
    }

    public static void OnPlay()
    {
        GameplayController.LoadSceneByName(Instance._levelToPlay);
    }

    public static void SeeInstructions()
    {
        Instance._instructions.SetActive(true);
        Instance._mainScreen.SetActive(false);
        Instance._credits.SetActive(false);
    }

    public static void SeeMainScreen()
    {
        Instance._instructions.SetActive(false);
        Instance._mainScreen.SetActive(true);
        Instance._credits.SetActive(false);
    }

    public static void SeeCredits()
    {
        Instance._instructions.SetActive(false);
        Instance._mainScreen.SetActive(false);
        Instance._credits.SetActive(true);
    }

    public static void ExitGame()
    {
        Application.Quit();
    }


}
