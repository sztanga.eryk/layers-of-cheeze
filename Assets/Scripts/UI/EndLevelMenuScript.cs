using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting.Dependencies.NCalc;
using UnityEngine;

public class EndLevelMenuScript : MonoBehaviour
{
    [SerializeField]
    private GameObject _winText;
    [SerializeField]
    private GameObject _defeatText;
    [SerializeField]
    private TextMeshProUGUI _defeatedEnemies;

    public void SetMenu(bool victory)
    {
        if (victory)
        {
            _winText.SetActive(true);
            _defeatText.SetActive(false);
        }
        else
        {
            _winText.SetActive(false);
            _defeatText.SetActive(true);

        }
        SetText();
    }

    private void SetText()
    {
        var maxEnemies = LevelConditionManager.MaxEnemyCount;
        var defeatedEnemies = maxEnemies - LevelConditionManager.CurrEnemyCount;
        _defeatedEnemies.text = "Defeated enemies: " + defeatedEnemies + "/" + maxEnemies;
    }
}
