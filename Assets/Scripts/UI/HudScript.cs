using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HudScript : MonoBehaviour
{
    public static HudScript Instance { get; private set; }

    [SerializeField]
    private Image _firstHeart;
    [SerializeField]
    private float _partOfScreenBetweenHearts;
    [SerializeField]
    private Sprite _fullHeart;
    [SerializeField]
    private Sprite _halfHeart;
    [SerializeField]
    private Sprite _emptyHeart;
    private List<Image> _hearts = new();
    private int _maxHearts = 1;

    [Space]
    [SerializeField]
    private Image _pointer;

    [Space]
    [SerializeField]
    private Image _dashIcon;
    [SerializeField]
    [Range(0, 1)]
    private float _dashOffAlfa = 0.1f;

    [Space]
    [SerializeField]
    private Slider _objectiveSlider;
    [SerializeField]
    private TMP_Text _objectiveText;


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        _objectiveSlider.interactable = false;
        Instance._hearts.Add(Instance._firstHeart);
    }

    public static void InitMaxHealth(int maxHP)
    {
        Vector3 translation = new(Screen.width * Instance._partOfScreenBetweenHearts, 0.0f, 0.0f);
        int newMaxHearts = maxHP % 2 == 0 ? maxHP / 2 : maxHP / 2 + 1;
        if (newMaxHearts > Instance._maxHearts)
        {
            Vector3 lastPosition = Instance._hearts.Last().transform.position;
            for (int i = Instance._maxHearts; i < newMaxHearts; i++)
            {
                var newHeart = Instantiate(Instance._firstHeart, lastPosition + translation, Instance._firstHeart.transform.rotation, Instance._firstHeart.transform.parent);
                lastPosition = newHeart.transform.position;
                Instance._hearts.Add(newHeart);
            }
        }
        else if (newMaxHearts < Instance._maxHearts)
        {
            for (int i = newMaxHearts; i < Instance._maxHearts; i++)
            {
                Destroy(Instance._hearts[i].gameObject);
            }
            Instance._hearts.RemoveRange(newMaxHearts, Instance._maxHearts - newMaxHearts);
        }
        Instance._maxHearts = newMaxHearts;
    }

    public static void UpdateHealth(int hp)
    {
        for (int i = 0; i < Instance._maxHearts; i++)
        {
            if (i < hp / 2)
            {
                Instance._hearts[i].sprite = Instance._fullHeart;
            }
            else if (i == hp / 2 && hp % 2 == 1)
            {

                Instance._hearts[i].sprite = Instance._halfHeart;
            }
            else
            {
                Instance._hearts[i].sprite = Instance._emptyHeart;
            }
        }
    }

    public static void SetPointer(Vector2 pos)
    {
        Instance._pointer.transform.position = pos;
    }

    public static void SetDash(bool dashEnabled)
    {
        Color c = Instance._dashIcon.color;
        c.a = dashEnabled ? 1.0f : Instance._dashOffAlfa;
        Instance._dashIcon.color = c;
    }

    public static void SetActive(bool state)
    {
        Instance.GetComponent<CanvasGroup>().alpha = state ? 1 : 0;
    }

    public static void SetObjective(int currentValue, int maxValue)
    {
        Instance._objectiveSlider.maxValue = maxValue;
        Instance._objectiveSlider.value = currentValue;
        Instance._objectiveText.text = $"{currentValue} / {maxValue}";
    }
}
