using UnityEngine;

public interface IPushable
{
    public void Push(Vector2 direction, float pushSpeed, float pushTime);
}
