using UnityEngine;
using UnityEngine.InputSystem;

public class InputManagerScript : MonoBehaviour
{

    public Vector2 InputTargetLocation { get; private set; }
    private Vector2 _inputScreenLocation;
    private Vector2 _inputMoveVector;
    private PlayerInput _input;

    private void Start()
    {
        _input = GetComponent<PlayerInput>();
    }


    private void Update()
    {
        InputTargetLocation = Camera.main.ScreenToWorldPoint(_inputScreenLocation);
        if (GameplayController.LaserShooter)
            GameplayController.LaserShooter.GetComponent<LaserScript>().UpdateMousePosition(InputTargetLocation); // only for debug
    }

    private void FixedUpdate()
    {
        if (GameplayController.Player)
            GameplayController.Player.GetComponent<PlayerMovement>().MovePlayer(_inputMoveVector);

    }

    public void OnMove(InputValue value)
    {
        _inputMoveVector = value.Get<Vector2>().normalized;
    }

    public void OnDash()
    {
        GameplayController.Player.GetComponent<PlayerMovement>().PrepareDash();
    }

    public void OnLaserFire()
    {
        GameplayController.LaserShooter.GetComponent<LaserScript>().Shoot(InputTargetLocation);
    }

    public void OnGunFire()
    {
        GameplayController.Gun.GetComponent<GunScript>().Shoot(InputTargetLocation);
    }

    public void OnAim(InputValue value)
    {
        var screenLoc = value.Get<Vector2>();
        if (screenLoc != Vector2.zero)
        {
            _inputScreenLocation = screenLoc;
            HudScript.SetPointer(screenLoc);
        }
    }

    public void OnPause()
    {
        GameplayController.Pause();
    }

    public void DisableInput()
    {
        _input ??= GetComponent<PlayerInput>();
        _input.DeactivateInput();
    }

    public void EnableInput()
    {
        _input.ActivateInput();
    }

    public void SwitchInputMap(string map)
    {
        _input.SwitchCurrentActionMap(map);
    }
}
