using System;
using System.Collections;
using System.Data;
using UnityEditor;
using UnityEditor.Rendering.Universal;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameplayController : MonoBehaviour
{
    public static GameplayController Instance { get; private set; }
    public static GameObject Player { get; private set; }
    public static GameObject LaserShooter { get; private set; }
    public static GameObject Gun { get; private set; }
    private enum GameState
    {
        Paused,
        Playing,
        Loading,
        Menu,
        EndLevel,
        LoadingEnd
    }

    private static string _startingScene;
    private static string _currentScene;
    [SerializeField]
    private string _mainMenuScene;
    [SerializeField]
    private float _waitAtEnd = 0.8f;
    [SerializeField]
    private float _waitAtStart = 0.8f;
    [Header("Game Prefabs")]
    public GameObject PlayerPrefab;
    public GameObject LaserPrefab;
    public GameObject GunPrefab;
    public GameObject CameraPrefab;

    [Header("Menu Prefabs")]
    [SerializeField]
    private GameObject _loadingMenu;
    [SerializeField]
    private GameObject _pauseMenu;
    [SerializeField]
    private GameObject _menuIndicator;
    [SerializeField]
    private GameObject _endMenu;
    // alternatywnie można zrobić tak jak z _input czyli pole statyczne, szukane po tagu
    // zserializowany prywatny obiekt do ustawinia w edytorze jest chyba lepszy, 
    //jeśli bedzie się chciało mieć 2 lub więcej i je porównywać w edytorzr
    private static InputManagerScript _input;
    private static GameState _state = GameState.Loading;


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        _startingScene = SceneManager.GetActiveScene().name;
        GameObject inputObj = GameObject.FindWithTag("InputManager");
        _input = inputObj.GetComponent<InputManagerScript>();
        AtStart();
    }

    private static void SetPlayingState()
    {
        AudioManager.ResetVolumeAll();
        AudioManager.PlayLoopedOrContinue("game-bgm");
        PlayerCameraScript.SetCameraFollow(true);
        HudScript.SetActive(true);
        _input.EnableInput();
        _input.SwitchInputMap("Player");
        Time.timeScale = 1;
        Instance._pauseMenu.SetActive(false);
        Instance._loadingMenu.SetActive(false);
        Instance._menuIndicator.SetActive(false);
        Instance._endMenu.SetActive(false);
        _state = GameState.Playing;
    }

    private static void SetPausedState()
    {
        AudioManager.SetPauseVolume();
        HudScript.SetActive(true);
        _input.EnableInput();
        _input.SwitchInputMap("Menu");
        Time.timeScale = 0;
        Instance._pauseMenu.SetActive(true);
        Instance._loadingMenu.SetActive(false);
        Instance._menuIndicator.SetActive(false);
        Instance._endMenu.SetActive(false);
        _state = GameState.Paused;
    }

    private static void SetLoadingState()
    {
        AudioManager.StopAll();
        AudioManager.ResetVolumeAll();
        HudScript.SetActive(false);
        _input.DisableInput();
        Time.timeScale = 0;
        Instance._pauseMenu.SetActive(false);
        Instance._loadingMenu.SetActive(true);
        Instance._menuIndicator.SetActive(false);
        Instance._endMenu.SetActive(false);
        _state = GameState.Loading;
    }
    private static void SetMenuState()
    {
        AudioManager.ResetVolumeAll();
        AudioManager.PlayLooped("menu-bgm");
        PlayerCameraScript.SetCameraFollow(false);
        HudScript.SetActive(false);
        _input.EnableInput();
        _input.SwitchInputMap("Menu");
        Time.timeScale = 0;
        Instance._pauseMenu.SetActive(false);
        Instance._loadingMenu.SetActive(false);
        Instance._menuIndicator.SetActive(true);
        Instance._endMenu.SetActive(false);
        _state = GameState.Menu;
    }

    private static void SetEndLevelState()
    {
        AudioManager.ResetVolumeAll();
        AudioManager.StopAllForeginAfterRealtime(1.4f);
        HudScript.SetActive(false);
        _input.EnableInput();
        Time.timeScale = 0;
        _input.SwitchInputMap("Menu");
        Instance._pauseMenu.SetActive(false);
        Instance._loadingMenu.SetActive(false);
        Instance._menuIndicator.SetActive(false);
        Instance._endMenu.SetActive(true);
        _state = GameState.EndLevel;
    }

    private static void SetLoadingEndState()
    {
        AudioManager.ResetVolumeAll();
        _input.DisableInput();
        Time.timeScale = 1;
        Instance._pauseMenu.SetActive(false);
        Instance._loadingMenu.SetActive(false);
        Instance._menuIndicator.SetActive(false);
        Instance._endMenu.SetActive(false);
        _state = GameState.LoadingEnd;

    }

    private static IEnumerator WaitAtEnd(bool victory)
    {
        yield return new WaitForSeconds(Instance._waitAtEnd);
        Player.SetActive(false);
        SetEndLevelState();
        Instance._endMenu.GetComponent<EndLevelMenuScript>().SetMenu(victory);
    }

    private static void EndGame(bool victory)
    {
        SetLoadingEndState();
        Instance.StartCoroutine(WaitAtEnd(victory));
    }

    public static void Pause()
    {
        if (_state == GameState.Playing)
        {
            SetPausedState();
        }
        else if (_state == GameState.Paused)
        {
            SetPlayingState();
        }
    }

    public static void PlayerDied()
    {
        AudioManager.PlayOnce("playerdeath");
        LevelConditionManager.MakeEnemiesInvulnerable();
        EndGame(false);
    }

    public static void PlayerWon()
    {
        Player.GetComponent<PlayerHealthBehaviour>().SetVulnerability(false);
        EndGame(true);
    }

    public static void ReloadScene()
    {
        LoadSceneByName(_currentScene);
    }

    public static void LoadMainMenu()
    {
        LoadSceneByName(Instance._mainMenuScene);
    }

    public static void LoadSceneByName(string name)
    {
        SetLoadingState();
        Player = null;
        LaserShooter = null;
        Gun = null;
        _currentScene = name;
        Instance.StartCoroutine(LoadScene(name));
    }

    public static void AtStart()
    {
        SetLoadingState();
        Player = null;
        LaserShooter = null;
        Gun = null;
        _currentScene = _startingScene;
        Instance.StartCoroutine(LoadAtStart());
    }

    private static void DetermineSceneState()
    {
        GameObject spawn = GameObject.FindWithTag("PlayerSpawn");
        if (spawn)
        {
            LoadPlayer(spawn);
            SetPlayingState();
        }
        else
        {
            SetMenuState();
        }
    }

    private static void LoadPlayer(GameObject spawn)
    {
        Player = Instantiate(Instance.PlayerPrefab, spawn.transform.position, spawn.transform.rotation);
        LaserShooter = Instantiate(Instance.LaserPrefab, spawn.transform.position, spawn.transform.rotation, Player.transform);
        Gun = Instantiate(Instance.GunPrefab, spawn.transform.position, spawn.transform.rotation, Player.transform);
        Instantiate(Instance.CameraPrefab, spawn.transform.position, spawn.transform.rotation);
    }

    private static IEnumerator LoadScene(string sceneName)
    {
        LevelConditionManager.ResetEnemyCount();
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
        yield return new WaitForSecondsRealtime(0.5f);
        DetermineSceneState();
    }
    private static IEnumerator LoadAtStart()
    {
        LevelConditionManager.ResetEnemyCount();
        yield return new WaitForSecondsRealtime(Instance._waitAtStart);
        DetermineSceneState();
    }

}
