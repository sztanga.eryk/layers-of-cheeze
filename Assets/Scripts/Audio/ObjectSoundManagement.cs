using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSoundScript : MonoBehaviour
{
    [SerializeField]
    private float _minDistance = 3f;
    [SerializeField]
    private float _spatialBlend = 0.85f;
    private float _dopplerLevel = 0f;
    private float _timeToSelfDestruct = 2f;
    public List<Sound> sounds;
    // Start is called before the first frame update
    void Start()
    {
        foreach (var s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.volume = s.volume;
            s.source.clip = s.clip;
            s.source.minDistance = _minDistance;
            s.source.spatialBlend = _spatialBlend;
            s.source.dopplerLevel = _dopplerLevel;
            s.source.rolloffMode = AudioRolloffMode.Logarithmic;
            AudioManager.AddSound(s);
        }
    }
    void OnDestroy()
    {
        foreach (var s in sounds)
        {
            AudioManager.RemoveSound(s);
        }
    }

    public void Detach()
    {
        gameObject.transform.parent = null;
        Destroy(gameObject, _timeToSelfDestruct);
    }

    public void PlayOnce(string name)
    {
        var sound = sounds.Find(sound => sound.name == name).source;
        if (sound == null)
        {
            Debug.LogWarning(name + " sound does not exist.");
            return;
        }
        sound.loop = false;
        sound.Play();
    }

    public void PlayLooped(string name)
    {
        var sound = sounds.Find(sound => sound.name == name).source;
        if (sound == null)
        {
            Debug.LogWarning(name + " sound does not exist.");
            return;
        }
        sound.loop = true;
        sound.Play();
    }

    public void Stop(string name)
    {
        var sound = sounds.Find(sound => sound.name == name).source;
        if (sound == null)
        {
            Debug.LogWarning(name + " sound does not exist.");
            return;
        }
        sound.Stop();
    }
}
