using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField]
    private float _pauseVolume = 0.25f;
    public static AudioManager Instance { get; private set; }
    public List<Sound> sounds;
    public List<Sound> foreginSounds;


    private void Awake()
    {
        if (Instance && Instance != this)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);

        foreach (var s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.volume = s.volume;
            s.source.clip = s.clip;
        }
    }

    public static void PlayOnce(string name)
    {
        Sound s = Instance.sounds.Find(sound => sound.name == name);
        if (s == null)
        {
            LogAbsentSoundWarning(name);
            return;
        }
        s.source.loop = false;
        s.source.Play();
    }

    public static void PlayLooped(string name)
    {
        Sound s = Instance.sounds.Find(sound => sound.name == name);
        if (s == null)
        {
            LogAbsentSoundWarning(name);
            return;
        }
        s.source.loop = true;
        s.source.Play();
    }

    public static void PlayLoopedOrContinue(string name)
    {
        Sound s = Instance.sounds.Find(sound => sound.name == name);
        if (s == null)
        {
            LogAbsentSoundWarning(name);
            return;
        }
        if (!s.source.isPlaying)
        {
            s.source.loop = true;
            s.source.Play();
        }
    }

    public static void Stop(string name)
    {
        Sound s = Instance.sounds.Find(sound => sound.name == name);
        if (s == null)
        {
            LogAbsentSoundWarning(name);
            return;
        }
        s.source.Stop();
    }

    public static void StopAll()
    {
        foreach (var s in Instance.sounds)
        {
            s.source.Stop();
        }
        foreach (var s in Instance.foreginSounds)
        {
            s.source.Stop();
        }
    }

    public static void StopAllForeginAfterRealtime(float seconds)
    {
        Instance.StartCoroutine(Instance.WaitAndStopAllForegin(seconds));
    }

    private IEnumerator WaitAndStopAllForegin(float seconds)
    {
        yield return new WaitForSecondsRealtime(seconds);
        foreach (var s in Instance.foreginSounds)
        {
            s.source.Stop();
        }

    }

    public static void ResetVolumeAll()
    {
        foreach (var s in Instance.sounds)
        {
            s.source.volume = s.volume;
        }
        foreach (var s in Instance.foreginSounds)
        {
            s.source.volume = s.volume;
        }
    }

    public static void ScaleVolumeAll(float mult)
    {
        foreach (var s in Instance.sounds)
        {
            s.source.volume *= mult;
        }
        foreach (var s in Instance.foreginSounds)
        {
            s.source.volume *= mult;
        }
    }

    public static void SetPauseVolume()
    {
        ScaleVolumeAll(Instance._pauseVolume);
    }


    public static void Pause(string name)
    {
        Sound s = Instance.sounds.Find(sound => sound.name == name);
        if (s == null)
        {
            LogAbsentSoundWarning(name);
            return;
        }
        s.source.Pause();
    }

    public static void AddSound(Sound s)
    {
        Instance.foreginSounds.Add(s);
    }

    public static void RemoveSound(Sound s)
    {
        Instance.foreginSounds.Remove(s);
    }

    private static void LogAbsentSoundWarning(string name)
    {
        Debug.LogWarning("Sound: " + name + " not found");
    }
}
