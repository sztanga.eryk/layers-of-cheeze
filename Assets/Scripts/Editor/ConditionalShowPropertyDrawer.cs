using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(ConditionalShowAttribute))]
public class ConditionalShowPropertyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        ConditionalShowAttribute condHideAttribute = (ConditionalShowAttribute)attribute;
        bool enabled = CheckIfVisible(condHideAttribute, property);

        bool wasEnabled = GUI.enabled;
        GUI.enabled = enabled;
        if (enabled)
        {
            EditorGUI.PropertyField(position, property, label, true);
        }

        GUI.enabled = wasEnabled;
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        ConditionalShowAttribute condHideAttribute = (ConditionalShowAttribute)attribute;
        bool enabled = CheckIfVisible(condHideAttribute, property);

        if (enabled)
        {
            return EditorGUI.GetPropertyHeight(property, label);
        }
        else
        {
            return -EditorGUIUtility.standardVerticalSpacing;
        }
    }

    private bool CheckIfVisible(ConditionalShowAttribute condHideAttribute, SerializedProperty property)
    {
        string propertyPath = property.propertyPath;
        string conditionPath = propertyPath.Replace(property.name, condHideAttribute.sourceField);
        SerializedProperty sourcePropertyValue = property.serializedObject.FindProperty(conditionPath);

        if (sourcePropertyValue != null)
        {
            return (Command.CommandType)sourcePropertyValue.enumValueIndex == condHideAttribute.commandType;
        }
        else
        {
            Debug.LogWarning("Attempting to use a ConditionalShowAttribute but no matching SourcePropertyValue found in object: " + condHideAttribute.sourceField);
        }
        return true;
    }
}