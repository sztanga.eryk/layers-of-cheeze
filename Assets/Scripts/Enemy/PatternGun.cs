using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatternGun : MonoBehaviour
{
    [SerializeField]
    private Pattern _gunPattern;
    private int _currentCommand = 0;
    private bool _isWaiting = false;
    private bool _firstShot = true;
    private GameObject _target;
    [SerializeField]
    private ObjectSoundScript _soundsObject;
    private IEnumerator Wait(Command waitCommand)
    {
        _isWaiting = true;
        yield return new WaitForSeconds(waitCommand.WaitTime);
        _isWaiting = false;
    }

    private void Shoot(Command shootCommand)
    {
        string randomShot = "gunshot" + Random.Range(0, 8);
        _soundsObject.PlayOnce(randomShot);
        var newBullet = Instantiate(shootCommand.Bullet, transform.position, transform.rotation);
        var direction = CalculateDirection(shootCommand);
        newBullet.GetComponent<BulletBehaviour>().Shoot(direction * shootCommand.ShootForce);
    }

    private Vector2 CalculateDirection(Command shootCommand)
    {
        var toTargetDirection = ((Vector2)_target.transform.position - (Vector2)transform.position).normalized;
        float missAngle;
        if (_firstShot)
        {
            var sign = Random.Range(0, 2) * 2 - 1;
            missAngle = sign * shootCommand.EpsilonAngle * _gunPattern.firstShotMissFactor;
            _firstShot = false;
        }
        else
        {
            missAngle = Random.Range(-shootCommand.EpsilonAngle, shootCommand.EpsilonAngle);
        }
        var finalDirection = Quaternion.Euler(0.0f, 0.0f, shootCommand.Angle + missAngle) * toTargetDirection;
        return finalDirection;
    }

    private void ProcessNextCommand()
    {
        Command nextCommand = _gunPattern.commandList[_currentCommand];
        switch (nextCommand.Type)
        {
            case Command.CommandType.Wait:
                StartCoroutine(Wait(nextCommand));
                break;
            case Command.CommandType.Shoot:
                Shoot(nextCommand);
                break;
            default:
                Debug.Log($"{nextCommand.Type} not implemented");
                break;
        }
        _currentCommand = _currentCommand + 1 == _gunPattern.commandList.Length ? 0 : _currentCommand + 1;
    }

    public void SetNewTarget(GameObject newTarget)
    {
        _target = newTarget;
        _currentCommand = 0;
        _firstShot = true;
    }

    public void LoseTarget()
    {
        _target = null;
    }

    private void Update()
    {
        while (!_isWaiting && _target != null)
        {
            ProcessNextCommand();
        }
    }
}
