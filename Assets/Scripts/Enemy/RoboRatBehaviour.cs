using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;


namespace Enemy
{
    public class RoboRatBehaviour : MonoBehaviour, IPerceptive, IPushable
    {
        private Rigidbody2D _rigidbody;
        private Animator _animator;
        private bool _isObservingPlayer = false;
        private GameObject _target;
        [SerializeField]
        private float _distanceToKeep;
        [SerializeField]
        private float _distanceEpsilon;
        [SerializeField]
        private float _distanceZonesFallof;
        [SerializeField]
        private float _movementSpeed;
        [SerializeField]
        private float _towardsPlayerMovementWeight;
        [SerializeField]
        private float _rnMovementWeight;
        [SerializeField]
        private float _rnMovementChangeTime;
        [SerializeField]
        private float _rotationDeadZone = 2;
        private float _rotationTweenDeadZone = 15;
        [SerializeField]
        private float _rotationSpeed = .8f;
        [SerializeField]
        private GameObject _gun;

        private Vector2 _rnMovementVelocity;
        private Vector2 _lastMovementVector = Vector2.up;
        private int _rotationTweenId;
        private Vector2 _pushMovementVelocity;
        private bool _isPushed = false;

        public void Detect(GameObject other)
        {
            _target = other;
            _isObservingPlayer = true;
            _gun.GetComponent<PatternGun>().SetNewTarget(other);
        }

        public void Lose(GameObject other)
        {
            _isObservingPlayer = false;
            _gun.GetComponent<PatternGun>().LoseTarget();
        }

        private void Start()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
            _animator = GetComponent<Animator>();
            StartCoroutine(ManageRandomMovement());
            _rotationTweenId = LeanTween.rotateZ(gameObject, 0, 0.01f).uniqueId;
        }

        private void FixedUpdate()
        {
            var velocityToPlayer = GetVelocityTowardsPlayer();
            Vector2 movementVector = _isPushed ? _pushMovementVelocity : (_rnMovementVelocity + velocityToPlayer).normalized;
            _rigidbody.velocity = movementVector * _movementSpeed;
            if(movementVector != Vector2.zero) _animator.SetBool("IsMoving", true);
            else _animator.SetBool("IsMoving", false);
            var movementAngle = Vector2.Angle(movementVector, _lastMovementVector);
            if (movementVector != _lastMovementVector && movementVector != Vector2.zero)
            {
                if (movementAngle > _rotationDeadZone && !_isObservingPlayer)
                {
                    Rotate(movementVector);
                }
            }
            if (_isObservingPlayer)
            {
                Vector2 lookAt = _target.transform.position - transform.position;
                transform.up = lookAt;
            }
        }

        private Vector2 GetVelocityTowardsPlayer()
        {
            Vector2 velocityToPlayer = Vector2.zero;
            if (_isObservingPlayer)
            {
                Vector2 distanceFromPlayer = _target.transform.position - transform.position;
                if (distanceFromPlayer.magnitude > _distanceToKeep + _distanceEpsilon)
                {
                    var baseDistance = _distanceToKeep + _distanceEpsilon;
                    var fallofWeight = Mathf.InverseLerp(baseDistance, baseDistance + _distanceZonesFallof, distanceFromPlayer.magnitude);
                    velocityToPlayer = distanceFromPlayer.normalized * _towardsPlayerMovementWeight * fallofWeight;
                }
                else if (distanceFromPlayer.magnitude < _distanceToKeep - _distanceEpsilon)
                {
                    var baseDistance = _distanceToKeep - _distanceEpsilon;
                    var fallofWeight = 1 - Mathf.InverseLerp(baseDistance - _distanceZonesFallof, baseDistance, distanceFromPlayer.magnitude);
                    velocityToPlayer = -distanceFromPlayer.normalized * _towardsPlayerMovementWeight * fallofWeight;
                }
            }
            return velocityToPlayer;

        }

        private void Rotate(Vector2 movementVector)
        {
            var desiredRotation = Quaternion.LookRotation(transform.forward, movementVector);
            var tweenAngle = Quaternion.Angle(desiredRotation, transform.rotation);
            if (LeanTween.isTweening(_rotationTweenId))
            {
                var movementAngle = Vector2.Angle(movementVector, _lastMovementVector);
                if (movementAngle < _rotationTweenDeadZone)
                {
                    return;
                }
                LeanTween.cancel(_rotationTweenId);
            }
            _rotationTweenId = LeanTween.rotateZ(gameObject, desiredRotation.eulerAngles.z, tweenAngle / (_rotationSpeed * 360)).uniqueId;
            _lastMovementVector = movementVector;
        }


        private IEnumerator ManageRandomMovement()
        {
            while (true)
            {
                Vector2 randomMovementVector = Random.insideUnitCircle.normalized;
                _rnMovementVelocity = randomMovementVector * _rnMovementWeight;
                yield return new WaitForSeconds(_rnMovementChangeTime);
                _rnMovementVelocity = Vector2.zero;
                yield return new WaitForSeconds(_rnMovementChangeTime);
            }
        }

        public void Push(Vector2 direction, float pushSpeed, float pushTime)
        {
            StartCoroutine(DoPush(direction, pushSpeed, pushTime));
        }

        private IEnumerator DoPush(Vector2 direction, float pushSpeed, float pushTime)
        {
            _isPushed = true;
            _pushMovementVelocity = direction * pushSpeed;
            yield return new WaitForSeconds(pushTime);
            _isPushed = false;
        }
    }
}