using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnemyHealthBehaviour : HealthBehaviour
{
    [SerializeField]
    protected UnityEvent _creationEvent;
    [SerializeField]
    private ObjectSoundScript _soundsObject;

    protected override void Start()
    {
        base.Start();
        _creationEvent ??= new UnityEvent();
        _creationEvent.Invoke();
    }

    protected override void Kill()
    {
        //TODO: this is where I would like to put a robo-rat death sound
        _deathEvent.Invoke();
        _soundsObject.Detach();
        Destroy(gameObject);
    }
    public override void TakeDMG(int DMG, string attackerLayer)
    {
        base.TakeDMG(DMG, attackerLayer);
    }
    public override void PlayDMGSound(string layer)
    {
        if (layer.Equals("Bullet"))
        {
            string randomBang = "metalbang" + Random.Range(0, 6);
            _soundsObject.PlayOnce(randomBang);
        }
        else if (layer.Equals("Ball"))
        {
            string randomCrush = "crush" + Random.Range(0, 8);
            _soundsObject.PlayOnce(randomCrush);
        }
    }
}
