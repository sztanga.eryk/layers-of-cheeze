using UnityEngine;

namespace Enemy
{
    public class Perception : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            transform.parent.GetComponent<IPerceptive>().Detect(other.gameObject);
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            transform.parent.GetComponent<IPerceptive>().Lose(other.gameObject);
        }

    }
}
