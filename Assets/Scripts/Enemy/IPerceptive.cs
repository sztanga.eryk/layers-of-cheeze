using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemy
{
    public interface IPerceptive
    {
        void Detect(GameObject actor);
        void Lose(GameObject actor);
    }
}
