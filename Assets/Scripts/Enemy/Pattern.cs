using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PatternScriptableObject", order = 1)]
public class Pattern : ScriptableObject
{
    public string patternName;

    public float firstShotMissFactor;
    public Command[] commandList;

}
