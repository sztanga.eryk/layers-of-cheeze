using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace Enemy
{
    public class BallBehaviour : MonoBehaviour, IPerceptive
    {
        private enum State
        {
            Asleep,
            PreparingEnraged,
            Enraged,
            Chaotic
        }
        private GameObject _rageTarget;
        [SerializeField]
        private GameObject _sphere;
        [SerializeField]
        private float _asleepMass = 100f;
        [SerializeField]
        private float _enragedMass = 10f;
        [SerializeField]
        private float _chaoticMass = 5f;
        [SerializeField]
        private float _chargeDelay = 0.5f;
        [SerializeField]
        private float _chargeLength = 2f;
        [SerializeField]
        private float _chargeForce = 200f;
        [SerializeField]
        private Color _chargeColor = Color.red;
        [SerializeField]
        private float _chaosForce = 60f;
        [SerializeField]
        private float _chaosLength = 4f;
        [SerializeField]
        private Color _chaosColor = Color.blue;
        private Rigidbody2D _rigidbody;
        [SerializeField]
        private State _state;
        private bool _isObservingPlayer = false;
        [SerializeField]
        private GameObject _killbox;
        [SerializeField]
        private ObjectSoundScript _soundsObject;

        void Start()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
            BecomeAsleep();
        }

        void FixedUpdate()
        {
            if (_state != State.PreparingEnraged && _rigidbody.velocity.magnitude > 0)
            {
                Vector3 currentRotation = Vector3.Cross(_rigidbody.velocity, Vector3.forward);
                _sphere.transform.Rotate(currentRotation, Space.World);
            }
        }

        public void Detect(GameObject actor)
        {
            _isObservingPlayer = true;
            _rageTarget = actor;
            if (_state == State.Asleep)
            {

                BecomeEnraged();
            }
        }

        public void Lose(GameObject actor)
        {
            _isObservingPlayer = false;
        }

        private void BecomeEnraged()
        {
            _state = State.PreparingEnraged;
            _rigidbody.mass = _enragedMass;
            ChangeKillboxLayer("EnemyDamage");
            _sphere.GetComponent<MeshRenderer>().material.color = Color.black;
            PrepareCharge();
        }

        private void PrepareCharge()
        {
            _soundsObject.PlayOnce("ball-sleep-charge");
            _sphere.transform.LookAt(_rageTarget.transform.position);
            Vector2 dir = _rageTarget.transform.position - transform.position;
            Quaternion desiredRotation = Quaternion.LookRotation(transform.forward, dir);
            // tween time of rotation must be shorter than on color
            LeanTween.rotate(_sphere, new Vector3(0, 0, desiredRotation.eulerAngles.z + 90), _chargeDelay / 3);
            LeanTween.color(_sphere, _chargeColor, _chargeDelay).setEase(LeanTweenType.easeOutSine).setOnComplete(Charge);
        }

        private void Charge()
        {
            _soundsObject.PlayLooped("ball-ambience-charge");
            _state = State.Enraged;
            var target = _rageTarget.transform.position;
            Vector2 dir = target - transform.position;
            _rigidbody.drag = 0;
            _rigidbody.AddForce(dir.normalized * _chargeForce, ForceMode2D.Impulse);
            LeanTween.value(transform.gameObject, ActualizeDrag, 0f, 20f, _chargeLength)
                .setEase(LeanTweenType.easeInExpo)
                .setOnComplete(EndCharge);
        }

        private void EndCharge()
        {
            StopRollingSounds();
            _rigidbody.drag = 10;
            _rigidbody.velocity = Vector2.zero;
            if (!_isObservingPlayer)
            {
                _soundsObject.PlayOnce("ball-drift-sleep");
                BecomeAsleep();
            }
            else
            {
                BecomeEnraged();
            }
        }

        private void BecomeAsleep()
        {
            _state = State.Asleep;
            _rigidbody.mass = _asleepMass;
            _rigidbody.drag = 20;
            ChangeKillboxLayer("EnemyDamage");
            _sphere.GetComponent<MeshRenderer>().material.color = Color.white;
        }

        private void ActualizeDrag(float drag)
        {
            _rigidbody.drag = drag;
        }

        public void BecomeChaotic(Vector2 force)
        {
            StopRollingSounds();
            _soundsObject.PlayOnce("ball-hit");
            CancelTweens();
            _state = State.Chaotic;
            _rigidbody.mass = _chaoticMass;
            ChangeKillboxLayer("BallDamage");
            _sphere.GetComponent<MeshRenderer>().material.color = _chaosColor;
            PrepareChargeChaotic(force);
        }

        private void PrepareChargeChaotic(Vector2 force)
        {
            _soundsObject.PlayLooped("ball-ambience-charge");
            _rigidbody.drag = 0;
            _rigidbody.AddForce(-(_rigidbody.mass * _rigidbody.velocity), ForceMode2D.Impulse);
            CancelTweens();
            _rigidbody.AddForce(force * _chaosForce, ForceMode2D.Impulse);
            LeanTween.value(transform.gameObject, ActualizeDrag, 0f, 30f, _chaosLength)
                .setEase(LeanTweenType.easeInExpo)
                .setOnComplete(EndCharge);

        }

        private void ChangeKillboxLayer(string layerName)
        {
            _killbox.layer = LayerMask.NameToLayer(layerName);
        }

        private void CancelTweens()
        {
            if (LeanTween.isTweening(transform.gameObject))
            {
                LeanTween.cancel(transform.gameObject);
            }
            if (LeanTween.isTweening(_sphere))
            {
                LeanTween.cancel(_sphere);
            }

        }

        private void StopRollingSounds()
        {
            _soundsObject.Stop("ball-ambience-charge");
            _soundsObject.Stop("ball-ambience-drift");
        }
    }
}
