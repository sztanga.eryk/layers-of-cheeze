using System.ComponentModel;
using UnityEngine;

[System.Serializable]
public class Command
{
    public enum CommandType
    {
        Wait,
        Shoot
    }

    public CommandType Type;
    [ConditionalShow("Type", CommandType.Wait)]
    public float WaitTime;

    [ConditionalShow("Type", CommandType.Shoot)]
    public float ShootForce;
    [ConditionalShow("Type", CommandType.Shoot)]
    public float Angle;
    [ConditionalShow("Type", CommandType.Shoot)]
    public float EpsilonAngle;
    [ConditionalShow("Type", CommandType.Shoot)]
    public GameObject Bullet;

}
