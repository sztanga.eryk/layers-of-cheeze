using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Unity.Mathematics;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerMovement : MonoBehaviour, IPushable
{
    private Rigidbody2D _rigidBody;
    private TrailRenderer _trailRenderer;
    private SpriteRenderer _spriteRenderer;
    private Animator _animator;

    [SerializeField]
    private float _moveSpeed = 4f;
    [SerializeField]
    private Vector2 _movementVector;
    private Vector2 _lastMovement = new Vector2(0, 1);

    private bool _canDash = true;
    private bool _isDashing;
    [SerializeField]
    private float _dashDuration = .175f;
    [SerializeField]
    private float _dashSpeed = 14f;
    [SerializeField]
    private float _dashCooldown = 1f;
    [SerializeField]
    private float _blinkDuration = .2f;
    private Color _originalColor;
    private Vector2 _lastRotationVector = Vector2.zero;
    [SerializeField]
    private float _rotationSpeed = 2;
    private int _rotationTweenId;
    private bool _isPushed = false;



    //Start is called before the first frame update
    void Awake()
    {
        _rigidBody = gameObject.GetComponent<Rigidbody2D>();
        _trailRenderer = gameObject.GetComponent<TrailRenderer>();
        _spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        _animator = gameObject.GetComponent<Animator>();
        _originalColor = _spriteRenderer.color;
        HudScript.SetDash(true);
    }

    private void Start()
    {
        _rotationTweenId = LeanTween.rotateZ(gameObject, 0, 0.01f).uniqueId;
    }

    private IEnumerator Dash()
    {
        var health = GetComponent<PlayerHealthBehaviour>();
        _canDash = false;
        HudScript.SetDash(false);
        _isDashing = true;
        health.SetVulnerability(false);
        _rigidBody.velocity = _lastMovement * _dashSpeed;
        _trailRenderer.emitting = true;
        yield return new WaitForSeconds(_dashDuration);
        _trailRenderer.emitting = false;
        _isDashing = false;
        health.SetVulnerability(true);
        yield return new WaitForSeconds(_dashCooldown);
        _canDash = true;
        HudScript.SetDash(true);
        _spriteRenderer.color = Color.Lerp(_originalColor, new Color(0, 0, 0), .3f);
        yield return new WaitForSeconds(_blinkDuration);
        _spriteRenderer.color = _originalColor;
    }

    private void PrepareRotation()
    {
        var rotationVector = _movementVector;
        if (rotationVector != Vector2.zero && !rotationVector.Equals(_lastRotationVector))
        {
            _lastRotationVector = rotationVector;
            RotatePlayer(rotationVector);
        }
    }

    public void RotatePlayer(Vector2 desiredRotationVector)
    {
        var desiredRotation = Quaternion.LookRotation(transform.forward, desiredRotationVector);
        if (LeanTween.isTweening(_rotationTweenId))
        {
            LeanTween.cancel(_rotationTweenId);
        }
        var tweenTime = Quaternion.Angle(desiredRotation, transform.rotation);
        _rotationTweenId = LeanTween.rotateZ(gameObject, desiredRotation.eulerAngles.z, tweenTime / (_rotationSpeed * 360)).uniqueId;

    }

    public void PrepareDash()
    {
        if (_canDash)
        {
            StartCoroutine(Dash());
        }
    }

    public void MovePlayer(Vector2 movementVector)
    {
        if (_isDashing || _isPushed)
        {
            return;
        }
        _movementVector = movementVector;
        PrepareRotation();
        _rigidBody.velocity = _movementVector * _moveSpeed;
        if (_movementVector != Vector2.zero)
        {
            _lastMovement = _movementVector;
            _movementVector = Vector2.zero;
            _animator.SetBool("IsMoving", true);
        }
        else
        {
            _animator.SetBool("IsMoving", false);
        }
    }

    public void Push(Vector2 direction, float pushSpeed, float pushTime)
    {
        if (!_isDashing)
        {
            StartCoroutine(DoPush(direction, pushSpeed, pushTime));
        }
    }

    private IEnumerator DoPush(Vector2 direction, float pushSpeed, float pushTime)
    {
        _isPushed = true;
        _canDash = false;
        _rigidBody.velocity = direction * pushSpeed;
        yield return new WaitForSeconds(pushTime);
        _canDash = true;
        _isPushed = false;
    }

}
