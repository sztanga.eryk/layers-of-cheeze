using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealthBehaviour : HealthBehaviour
{
    private bool _isDead = false;
    [SerializeField]
    private float _invulnerabilityTime = 0.5f;


    protected override void Start()
    {
        HudScript.InitMaxHealth(_maxHP);
        base.Start();
    }

    public override void TakeDMG(int dmg, string attackerLayer)
    {
        if (_isVulnerable)
        {
            base.TakeDMG(dmg, attackerLayer);
            _isVulnerable = false;
            StartCoroutine(BeInvulnerable());
        }
    }

    public override void PlayDMGSound(string layer)
    {
        if (layer.Equals("Bullet")) AudioManager.PlayOnce("playerhit" + Random.Range(0, 8));
        else if (layer.Equals("Ball")) AudioManager.PlayOnce("crush" + Random.Range(0, 8));
    }


    private IEnumerator BeInvulnerable()
    {
        yield return new WaitForSeconds(_invulnerabilityTime);
        _isVulnerable = true;
    }

    public void ChangeMaxHP(int newMaxHP)
    {
        _maxHP = newMaxHP;
        _HP = _HP > _maxHP ? _maxHP : _HP;
        HudScript.InitMaxHealth(_maxHP);
        _healthEvent.Invoke(_HP);
    }

    [ContextMenu("TestChangeMaxHP")]
    public void TestChangeMaxHP()
    {
        ChangeMaxHP(Random.Range(1, 13));
    }

    protected override void Kill()
    {
        if (!_isDead)
        {
            _isDead = true;
            LookDead();
            base.Kill();
        }
    }

    public void LookDead()
    {
        GetComponent<SpriteRenderer>().color = Color.red;
    }

}
