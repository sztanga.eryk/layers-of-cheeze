using System.Collections;
using System.Collections.Generic;
using Enemy;
using UnityEngine;

public class LaserScript : MonoBehaviour
{
    [SerializeField]
    private float _laserForce = 1.0f;
    [SerializeField]
    private float _laserDistance = 50.0f;
    [SerializeField]
    private float _cooldown = 1.0f;
    [SerializeField]
    private int _laserFrames = 20;
    [SerializeField]
    private bool _simulatedPhysics = true;

    private LineRenderer _lineRenderer;
    private bool _canShoot = true;

    // for debug only
    private Vector2 _mousePosition;

    private void Start()
    {
        _lineRenderer = GetComponent<LineRenderer>();
    }

    private void Update()
    {
    }

    public void Shoot(Vector2 mousePosition)
    {
        if (_canShoot)
        {
            AudioManager.PlayOnce("laser");
            StartCoroutine(ExecuteShot(mousePosition));
        }
    }

    private IEnumerator ExecuteShot(Vector2 mousePosition)
    {
        Vector2 shooterPosition = transform.position;
        LayerMask ballWallMask = LayerMask.GetMask("Ball", "Wall");
        bool hit = false;
        for (int i = 0; i < _laserFrames; ++i)
        {
            if (!hit)
            {
                RaycastHit2D raycastHit = Physics2D.Raycast(shooterPosition, (mousePosition - shooterPosition).normalized, _laserDistance, ballWallMask);
                if (!raycastHit)
                {
                    continue;
                }
                GameObject hitObject = raycastHit.transform.gameObject;
                UpdateLaser(shooterPosition, raycastHit.point);
                if (hitObject.layer == LayerMask.NameToLayer("Ball"))
                {
                    Vector2 ballForce = CalculateForce(raycastHit);
                    hitObject.GetComponent<BallBehaviour>().BecomeChaotic(ballForce);
                    hit = true;
                }
            }
            yield return 0;
        }
        _lineRenderer.enabled = false;
        StartCoroutine(Cooldown());
    }

    private IEnumerator Cooldown()
    {
        _canShoot = false;
        yield return new WaitForSeconds(_cooldown);
        _canShoot = true;
    }

    private Vector2 CalculateForce(RaycastHit2D hitBall)
    {
        Vector2 radiusVector = (Vector2)hitBall.transform.position - hitBall.point;
        if (_simulatedPhysics)
        {
            Vector2 laserVector = hitBall.point - (Vector2)transform.position;
            float angle = Vector2.Angle(radiusVector, laserVector);
            return radiusVector.normalized * Mathf.Cos(Mathf.Deg2Rad * angle) * _laserForce;
        }
        else
        {
            return radiusVector.normalized * _laserForce;
        }
    }


    private void UpdateLaser(Vector2 start, Vector2 end)
    {
        _lineRenderer.enabled = false;
        _lineRenderer.SetPosition(0, start);
        _lineRenderer.SetPosition(1, end);
        _lineRenderer.enabled = true;
    }

    // only needed for debug
    public void UpdateMousePosition(Vector2 mousePosition)
    {
        _mousePosition = mousePosition;
    }

    void OnDrawGizmos()
    {
        Vector2 shooterPosition = transform.position;
        Debug.DrawRay(shooterPosition, (_mousePosition - shooterPosition).normalized * _laserDistance, Color.black);
        LayerMask ballMask = LayerMask.GetMask("Ball");
        RaycastHit2D hitObject = Physics2D.Raycast(shooterPosition, (_mousePosition - shooterPosition).normalized, _laserDistance, ballMask);
        if (hitObject.collider != null)
        {
            Gizmos.color = Color.black;
            Gizmos.DrawLine(hitObject.transform.position, hitObject.point);
        }
    }

}