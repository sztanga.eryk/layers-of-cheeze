using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelConditionManager : MonoBehaviour
{

    public static int MaxEnemyCount { get; private set; }
    public static int CurrEnemyCount { get; private set; }


    private void Start()
    {
        MaxEnemyCount = 0;
        CurrEnemyCount = 0;
    }

    public static void RegisterEnemy()
    {
        ++MaxEnemyCount;
        ++CurrEnemyCount;
        HudScript.SetObjective(MaxEnemyCount - CurrEnemyCount, MaxEnemyCount);
    }

    public static void ResetEnemyCount()
    {
        MaxEnemyCount = 0;
        CurrEnemyCount = 0;
    }

    public static void RegisterEnemyDeath()
    {
        --CurrEnemyCount;
        if (CurrEnemyCount == 0)
        {
            GameplayController.PlayerWon();
        }
        HudScript.SetObjective(MaxEnemyCount - CurrEnemyCount, MaxEnemyCount);
    }

    public static void MakeEnemiesInvulnerable()
    {
        var enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (var e in enemies)
        {
            e.GetComponent<EnemyHealthBehaviour>().SetVulnerability(false);
        }
    }
}
