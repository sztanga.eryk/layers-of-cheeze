using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Animations;

public class GunScript : MonoBehaviour
{
    [SerializeField]
    private float _bulletForce = 10.0f;

    [SerializeField]
    private float _cooldown = 0.2f;
    [SerializeField]
    private GameObject _bullet;
    private float _cooldownTimer;

    private void Start()
    {
        _bullet.GetComponent<BulletBehaviour>().ChangeKillboxLayer(gameObject.layer);
    }

    private void Update()
    {
        _cooldownTimer -= Time.deltaTime;
    }

    public void Shoot(Vector2 targetPoint)
    {
        if (_cooldownTimer <= 0.0f)
        {
            AudioManager.PlayOnce("gunshot" + Random.Range(0, 8));
            var newBullet = Instantiate(_bullet, transform.position, transform.rotation);
            var direction = (targetPoint - (Vector2)transform.position).normalized;
            newBullet.GetComponent<BulletBehaviour>().Shoot(direction * _bulletForce);
            _cooldownTimer = _cooldown;
        }

    }
}
