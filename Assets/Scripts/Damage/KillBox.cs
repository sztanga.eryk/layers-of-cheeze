using UnityEngine;
using UnityEngine.Animations;

public class KillBox : MonoBehaviour
{
    [SerializeField]
    private int _DMG;
    [SerializeField]
    private float _pushspeed = 2.0f;
    [SerializeField]
    private float _pushTime = 0.2f;
    private void OnTriggerEnter2D(Collider2D other)
    {
        string layerName = LayerMask.LayerToName(transform.parent.gameObject.layer);
        other.transform.parent.GetComponent<HealthBehaviour>().TakeDMG(_DMG, layerName);
        Vector2 dir = transform.parent.GetComponent<Rigidbody2D>().velocity.normalized;
        other.transform.parent.GetComponent<IPushable>().Push(dir, _pushspeed, _pushTime);
    }
}
