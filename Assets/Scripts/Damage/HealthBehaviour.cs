using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HealthBehaviour : MonoBehaviour
{
    [SerializeField]
    protected int _maxHP = 6;
    [SerializeField]
    protected int _HP = 6;
    [SerializeField]
    protected float _hitTweenTime = 0.1f;
    protected int _hitTweenId;
    [SerializeField]
    protected UnityEvent _deathEvent;
    [SerializeField]
    protected UnityEvent<int> _healthEvent;
    protected Color _startColor;
    protected bool _isVulnerable = true;

    protected virtual void Start()
    {
        _deathEvent ??= new UnityEvent();
        _healthEvent ??= new UnityEvent<int>();
        _healthEvent.Invoke(_HP);
        _startColor = GetComponent<SpriteRenderer>().color;
        _hitTweenId = LeanTween.color(gameObject, _startColor, 0.01f).uniqueId;

    }
    public void SetVulnerability(bool value)
    {
        _isVulnerable = value;
    }

    public bool IsVulnerable()
    {
        return _isVulnerable;
    }

    public virtual void TakeDMG(int dmg, string attackerLayer)
    {
        PlayDMGSound(attackerLayer);

        _HP = _HP - dmg < 0 ? 0 : _HP - dmg;
        _healthEvent.Invoke(_HP);
        if (_HP <= 0)
        {
            Kill();
        }
        VisualiseHit();
    }

    public virtual void PlayDMGSound(string layer) { }

    public virtual void Heal(int addedHP)
    {
        _HP = _HP + addedHP > _maxHP ? _maxHP : _HP + addedHP;
        _healthEvent.Invoke(_HP);
    }

    [ContextMenu("TestHeal")]
    private void TestHeal()
    {
        Heal(1);
    }

    protected virtual void Kill()
    {
        _deathEvent.Invoke();
    }

    protected virtual void VisualiseHit()
    {
        if (LeanTween.isTweening(_hitTweenId))
        {
            LeanTween.cancel(_hitTweenId);
            GetComponent<SpriteRenderer>().color = _startColor;

        }
        _hitTweenId = LeanTween.color(gameObject, Color.black, _hitTweenTime).setLoopPingPong(1).uniqueId;

    }
}
