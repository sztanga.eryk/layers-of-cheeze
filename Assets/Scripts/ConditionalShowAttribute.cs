using System;
using UnityEngine;

// This attribute is writter speciflicly for using it in Command.cs
// If you what to use something similar to this this I sugest installing package
// that has similar attributes (f.e https://github.com/Deadcows/MyBox or https://github.com/HyagoOliveira/Attributes)
// I may rewrite this in the future to be more universal or install package
// that is doing what I'm doing but better and delete this attribute.
// But it isn't prioryty and I don't want to think about licences right now.

[AttributeUsage(AttributeTargets.Field)]
public class ConditionalShowAttribute : PropertyAttribute
{
    public string sourceField = "";

    public Command.CommandType commandType;

    public ConditionalShowAttribute(string sourceField, Command.CommandType commandType)
    {
        this.sourceField = sourceField;
        this.commandType = commandType;
    }
}
