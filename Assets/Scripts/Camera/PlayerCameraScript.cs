using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraScript : MonoBehaviour
{
    public static PlayerCameraScript Instance { get; private set; }
    private static GameObject _player;
    private static InputManagerScript _mouse;
    [SerializeField]
    private GameObject _middlePoint;
    [SerializeField]
    private GameObject _audioListener;
    private float _cameraDistanceMultiplier = 0.175f;
    private enum CameraState
    {
        Following,
        Standing
    }
    private static CameraState _cameraState = CameraState.Standing;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
    }
    void Start()
    {
        _mouse = GameObject.FindWithTag("InputManager").GetComponent<InputManagerScript>();
    }

    void FixedUpdate()
    {
        if(_cameraState == CameraState.Following)
        {
            SetMiddlePointPosition();
            SetAudioListenerPosition();
        }    
    }

    public static void SetCameraFollow(bool follow)
    {
        if(follow)
        {
            _cameraState = CameraState.Following;
            _player = GameplayController.Player;
        }else
        {
            _cameraState = CameraState.Standing;
            _player = null;
        }
    }

    private void SetAudioListenerPosition()
    {
        Instance._audioListener.transform.position = _player.transform.position;
    }

    private void SetMiddlePointPosition()
    {
        Vector2 mousePosition = _mouse.InputTargetLocation;
        Vector3 fromPlayerToMouse = new Vector3(mousePosition.x, mousePosition.y, 0) - _player.transform.position;
        Instance._middlePoint.transform.position = _player.transform.position + fromPlayerToMouse * _cameraDistanceMultiplier;
    }
}
